package br.com.gg.account.config;

import br.com.gg.account.domain.Account;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import br.com.gg.account.repository.AccountRepository;
import br.com.gg.account.repository.RoleRepository;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    private final AccountRepository accountRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public CustomUserDetailsService(AccountRepository userRepository, RoleRepository userRolesRepository) {
        this.accountRepository = userRepository;
        this.roleRepository = userRolesRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account user = accountRepository.findByUsername(username);
        if (null == user) {
            throw new UsernameNotFoundException("No user present with username: " + username);
        } else {
            List<String> userRoles = roleRepository.findRoleByUserId(user.getId());
            return new CustomUserDetails(user, userRoles);
        }
    }

}
