package br.com.gg.account.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author spider
 */
@Entity
public class Livro {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @NotEmpty(message = "O campo 'nome' não pode ser vazio")
    private String nome;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonFormat(pattern = "dd/mm/yyyy")
    @NotNull(message = "O campo 'publicação' é obrigatório")
    private Date publicacao;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @NotNull(message = "O campo 'editora' é obrigatório")
    private String editora;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @NotEmpty(message = "O campo 'resumo' não pode ser vazio")
    @Size(max = 1500, message = "O campo 'resumo' deve ter no máximo 1500")
    private String resumo;
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "livro")
    private List<Comentario> comantarios;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @NotNull(message = "O 'autor' do livro deve ser obrigatoriamente informado")
    @ManyToOne(fetch = FetchType.EAGER) // O default entre LAZY e EAGER é EAGER, se vc não especificar LAZY, sempre sará setado EAGER. Sempre que o Many quiser invocar o One da relaçao, so e possivel com EAGER
    @JoinColumn(name = "AUTOR_ID")
    private Autor autor;

    public Livro() {
    }

    public Livro(String nome) {
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getPublicacao() {
        return publicacao;
    }

    public void setPublicacao(Date publicacao) {
        this.publicacao = publicacao;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public List<Comentario> getComantarios() {
        return comantarios;
    }

    public void setComantarios(List<Comentario> comantarios) {
        this.comantarios = comantarios;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.id);
        hash = 89 * hash + Objects.hashCode(this.nome);
        hash = 89 * hash + Objects.hashCode(this.publicacao);
        hash = 89 * hash + Objects.hashCode(this.editora);
        hash = 89 * hash + Objects.hashCode(this.resumo);
        hash = 89 * hash + Objects.hashCode(this.comantarios);
        hash = 89 * hash + Objects.hashCode(this.autor);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Livro other = (Livro) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.editora, other.editora)) {
            return false;
        }
        if (!Objects.equals(this.resumo, other.resumo)) {
            return false;
        }
        if (!Objects.equals(this.publicacao, other.publicacao)) {
            return false;
        }
        if (!Objects.equals(this.comantarios, other.comantarios)) {
            return false;
        }
        if (!Objects.equals(this.autor, other.autor)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Livro{" + "id=" + id + ", nome=" + nome + ", publicacao=" + publicacao + ", editora=" + editora + ", resumo=" + resumo + ", comantarios=" + comantarios + ", autor=" + autor + '}';
    }
}
