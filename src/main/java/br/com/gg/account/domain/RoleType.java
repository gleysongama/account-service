package br.com.gg.account.domain;

/**
 *
 * @author spider
 */
public enum RoleType {
    USER("ROLE_USER"),
    DBA("ROLE_DBA"),
    ADMIN("ROLE_ADMIN");
     
    String roleType;
     
    private RoleType(String roleType){
        this.roleType = roleType;
    }
     
    public String getRoleType(){
        return roleType;
    }
     
}