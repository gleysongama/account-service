package br.com.gg.account.services.exception;

/**
 *
 * @author spider
 */
public class AutorExistenteException extends RuntimeException {
    
    public AutorExistenteException(String mensagem) {
        super(mensagem);
    }
    
    public AutorExistenteException(String mensagem, Throwable causa) {
        super(mensagem, causa);
    }
}
