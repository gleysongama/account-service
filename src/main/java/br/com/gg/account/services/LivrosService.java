package br.com.gg.account.services;

import br.com.gg.account.domain.Comentario;
import br.com.gg.account.domain.Livro;
import br.com.gg.account.repository.AutoresRepository;
import br.com.gg.account.repository.ComentariosRepository;
import br.com.gg.account.repository.LivrosRepository;
import br.com.gg.account.services.exception.LivroNaoEncontradoException;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

/**
 *
 * @author spider
 */
@Service
public class LivrosService {

    @Autowired
    private LivrosRepository livrosRepository;
    
    @Autowired
    private ComentariosRepository comentariosRepository;
    
    @Autowired
    private AutoresRepository autoresRepository;

    public List<Livro> listar() {
        return livrosRepository.findAll();
    }

    public Livro salvar(Livro livro) {
        livro.setId(null);
        return livrosRepository.save(livro);
    }

    public Livro buscar(Long id) {
        Livro livro = livrosRepository.findOne(id);
        if (livro == null) {
            throw new LivroNaoEncontradoException("O livro nao pode ser encontrado");
        }
        return livro;
    }

    public void deletar(Long id) {
        try {
            livrosRepository.delete(id);
        } catch (EmptyResultDataAccessException e) {
            throw new LivroNaoEncontradoException("O livro nao pode ser encontrado");
        }
    }

    public void deletarTodos() {
        try {
            livrosRepository.deleteAll();
        } catch (EmptyResultDataAccessException e) {
            throw new LivroNaoEncontradoException("Os livros nao podem ser encontrados");
        }
    }

    public void atualizar(Livro livro) {
        verificarExistencia(livro);
        livrosRepository.save(livro);
    }
    
    private void verificarExistencia(Livro livro) {
        buscar(livro.getId());
    }
    
    public Comentario adicionarComentario(Long id, Comentario comentario) {
        Livro livro = buscar(id);        
        comentario.setLivro(livro);
        comentario.setDataPostagem(new Date());        
        return comentariosRepository.save(comentario);
    }
    
    public List<Comentario> listarComentario(Long id) {
        Livro livro = buscar(id);
        return livro.getComantarios();
    }
    
}
