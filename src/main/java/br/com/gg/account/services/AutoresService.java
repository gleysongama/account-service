package br.com.gg.account.services;

import br.com.gg.account.domain.Autor;
import br.com.gg.account.repository.AutoresRepository;
import br.com.gg.account.services.exception.AutorExistenteException;
import br.com.gg.account.services.exception.AutorNaoEncontradoException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author spider
 */
@Service
public class AutoresService {
    
    @Autowired
    private AutoresRepository autoresRepository;
    
    public Autor salvar(Autor autor) {
        if(autor.getId() != null) {
            Autor a = autoresRepository.findOne(autor.getId());
            if(a != null) {
                 throw new AutorExistenteException("O autor ja existe");
            }
        }
        return autoresRepository.save(autor);
    }
    
    public List<Autor> listar() {
        return autoresRepository.findAll();
    }
    
    public Autor buscar(Long id) {
        Autor autor = autoresRepository.findOne(id);
        
        if(autor == null) {
            throw new AutorNaoEncontradoException("O autor nao foi encontrado");
        }
        return autoresRepository.findOne(id);
    }
    
    public void atualizar(Autor autor) {
        verificaExistencia(autor);
        autoresRepository.save(autor);
    }
    
    private void verificaExistencia(Autor autor) {
        buscar(autor.getId());
    }
    
    public void deletar(Long id) {
        autoresRepository.delete(id);
    }
    
    public void deletarTodos() {
        autoresRepository.deleteAll();
    }
}
