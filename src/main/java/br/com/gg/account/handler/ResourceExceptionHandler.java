package br.com.gg.account.handler;

import br.com.gg.account.domain.DetalhesErro;
import br.com.gg.account.services.exception.AutorExistenteException;
import br.com.gg.account.services.exception.AutorNaoEncontradoException;
import br.com.gg.account.services.exception.LivroNaoEncontradoException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author spider
 */
@ControllerAdvice
public class ResourceExceptionHandler {
    
    /* ERROS 404
     * @param e
     * @param request
     * @return */
    @ExceptionHandler(LivroNaoEncontradoException.class)
    public ResponseEntity<DetalhesErro> handleLivroNaoEncontradoException(LivroNaoEncontradoException e, HttpServletRequest request) {
        DetalhesErro erro = new DetalhesErro();

        erro.setStatus(404l);
        erro.setTitulo("O livro não pode ser encontrado");
        erro.setMensagemDesenvolvedor("http://erros.socialbooks.com/404"); // blog explicando causa e como resolver o erro
        erro.setTimestamp(System.currentTimeMillis());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
    }

    @ExceptionHandler(AutorNaoEncontradoException.class)
    public ResponseEntity<DetalhesErro> handlerAutorNaoEncontradoException(AutorNaoEncontradoException e, HttpServletRequest request) {
        DetalhesErro erro = new DetalhesErro();
        
        erro.setStatus(404l);
        erro.setTitulo("O autor não pode ser encontrado");
        erro.setMensagemDesenvolvedor("http://erros.socialbooks.com/404"); // blog explicando causa e como resolver o erro
        erro.setTimestamp(System.currentTimeMillis());
        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
    }
    
    /* ERROS 409
     * @param e
     * @param request
     * @return */
    @ExceptionHandler(AutorExistenteException.class)
    public ResponseEntity<DetalhesErro> handlerAutorExistenteException(AutorExistenteException e, HttpServletRequest request) {
        DetalhesErro erro = new DetalhesErro();
        
        erro.setStatus(409l);
        erro.setTitulo("O autor ja existente");
        erro.setMensagemDesenvolvedor("http://erros.socialbooks.com/409"); // blog explicando causa e como resolver o erro
        erro.setTimestamp(System.currentTimeMillis());
        
        return ResponseEntity.status(HttpStatus.CONFLICT).body(erro);
    }
    
    /* ERROS 400
     * @param e
     * @param request
     * @return */
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<DetalhesErro> handlerDataIntegrityViolationException(DataIntegrityViolationException e, HttpServletRequest request) {
        DetalhesErro erro = new DetalhesErro();
        
        erro.setStatus(400l);
        erro.setTitulo("O autor nao existe");
        erro.setMensagemDesenvolvedor("http://erros.socialbooks.com/400"); // blog explicando causa e como resolver o erro
        erro.setTimestamp(System.currentTimeMillis());
        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(erro);
    }
    
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<DetalhesErro> handlerHttpMessageNotReadableException(HttpMessageNotReadableException e, HttpServletRequest request) {
        DetalhesErro erro = new DetalhesErro();
        
        erro.setStatus(400l);
        erro.setTitulo("Envio de json com formato inválido");
        erro.setMensagemDesenvolvedor("http://erros.socialbooks.com/400"); // blog explicando causa e como resolver o erro
        erro.setTimestamp(System.currentTimeMillis());
        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(erro);
    }
}
