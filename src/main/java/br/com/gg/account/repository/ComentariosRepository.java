package br.com.gg.account.repository;

import br.com.gg.account.domain.Comentario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author spider
 */
public interface ComentariosRepository extends JpaRepository<Comentario, Long>{
    
}
