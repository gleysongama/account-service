package br.com.gg.account.repository;

import br.com.gg.account.domain.Role;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

    @Query("SELECT r.role FROM Authority a, Role r WHERE a.role.id = r.id AND a.account.id = ?1")
    public List<String> findRoleByUserId(Long userId);
}
