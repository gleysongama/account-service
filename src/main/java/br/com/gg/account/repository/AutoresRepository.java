package br.com.gg.account.repository;

import br.com.gg.account.domain.Autor;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author spider
 */
public interface AutoresRepository extends JpaRepository<Autor, Long>{
    
}
