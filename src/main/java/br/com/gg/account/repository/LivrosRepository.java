package br.com.gg.account.repository;

import br.com.gg.account.domain.Livro;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author spider
 */
public interface LivrosRepository extends JpaRepository<Livro, Long>{
    
}
