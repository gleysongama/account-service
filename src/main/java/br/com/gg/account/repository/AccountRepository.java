package br.com.gg.account.repository;

import br.com.gg.account.domain.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
    
    public Account findByUsername(String username);    
}